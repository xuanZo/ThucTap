

cc.Class({
    extends: cc.Component,

    properties: {
        id: 0,
        icon: cc.Sprite,
        spinCount: 0,
        index: 0,
        c: 10,
        heightFrame: 0,
        isSpinComplete: true,
    },
    init: function (data, index, heightFrame) {
        this.id = data.id;
        // this.icon = data.iconSF;
        this.node.getComponent(cc.Sprite).spriteFrame = data.iconSF;
        this.index = index;
        this.y = Math.floor(this.index / 5);
        this.heightFrame = heightFrame;
    },
    start() {
    },
    //ham de tao su kien quay
    startSpin: function () {
        this.time = 0;
        this.isSpinComplete = false;
        this.spinCount = 0;
        this.y0 = this.node.y;
    },
    ///ham dung su kien quay
    stopSpin() {
        this.isSpinComplete = true;
    },

    spin: function (dt) {
        this.time += dt;
        if (this.isSpinComplete) {
            return;
        }
        if (!this.isSpinComplete) {
            this.node.y = this.y0 - this.c * this.time;
            if (this.spinCount > 4) {
                if (this.index < 15) {
                    this.node.getComponent(cc.Sprite).spriteFrame = this.parent.setImage(this.parent.arrKQ[this.index]);
                }
                if (this.node.y < (this.y + 0.2) * this.heightFrame / 3) {
                    var moveUp = cc.moveTo(0.4 + Math.floor(this.index % 5) / 3, cc.v2(this.node.x, (this.y + 0.5) * this.heightFrame / 3));
                    // var moveDown = cc.moveTo(0.001, cc.v2(this.node.x, (this.y + 0.5) * this.heightFrame / 3))
                    this.node.runAction(moveUp);
                    // this.node.y = (this.y + 0.5) * this.heightFrame / 3;
                    this.isSpinComplete = true;
                }
            }
            if (this.node.y <= -this.node.height / 2) {
                const delta = -this.node.height / 2 - this.node.y;
                this.node.y = this.heightFrame * 7 / 6 - delta;
                this.y0 = this.node.y;
                this.time = 0;
                let idImage = Math.floor(Math.random() * 15);
                this.node.getComponent(cc.Sprite).spriteFrame = this.parent.setImage(idImage);
                this.spinCount++;
            }

        }
    },
    onLoad() {
        this.y = Math.floor(this.index / 5);
    },
    update(dt) {

        this.spin(dt);

    }

});
